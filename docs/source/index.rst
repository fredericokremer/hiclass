.. hiclass documentation master file, created by
   sphinx-quickstart on Tue Jul 20 12:37:27 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to hiclass' documentation!
===================================

.. image:: https://github.com/mirand863/hiclass/actions/workflows/deploy-pypi.yml/badge.svg?event=push
    :target: https://github.com/mirand863/hiclass/actions/workflows/deploy-pypi.yml
    :alt: Deploy PyPI

.. image:: https://readthedocs.org/projects/hiclass/badge/?version=latest
    :target: https://hiclass.readthedocs.io/en/latest/?badge=latest
    :alt: Documentation Status

.. image:: https://codecov.io/gh/mirand863/hiclass/branch/main/graph/badge.svg?token=PR8VLBMMNR
    :target: https://codecov.io/gh/mirand863/hiclass
    :alt: codecov

.. image:: https://img.shields.io/conda/dn/conda-forge/hiclass?label=conda
    :target: https://anaconda.org/conda-forge/hiclass
    :alt: Downloads Conda

.. image:: https://img.shields.io/pypi/dm/hiclass?label=pypi
    :target: https://pypi.org/project/hiclass/
    :alt: Downloads pypi

.. image:: https://img.shields.io/badge/License-BSD_3--Clause-blue.svg
    :target: https://opensource.org/licenses/BSD-3-Clause
    :alt: License

.. toctree::
    :titlesonly:

    introduction/index
    get_started/index
    algorithms/index

.. toctree::
   :maxdepth: 3

   api/index
