Learn how to use HiClass
========================

In the next few chapters, you will learn how to :ref:`Install HiClass` and set up your own hierarchical machine learning pipelines.

Once you are set up, we suggest working through our examples, including:

- A typical :ref:`A "Hello World" example`, for an entry-level description of the main concepts.
- A more detailed `tutorial <TODO>`_ to give you hands-on experience.

We also recommend the :ref:`API reference documentation` for additional information.
